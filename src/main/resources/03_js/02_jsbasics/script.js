'use strict';

// singleline comments
/* multiline comments */
console.log("__ TOPIC: DECLARING VARIABLES __");

var text1 = "abc"; // same as in Java: string text1 = "abc";
var text2 = 'abc';
console.log("Concatenating strings: " + text1 + "-" + text2);

var aggregate = 5 + text1 + "-" + text2 + "-something-else";
console.log(aggregate);

var number1 = 5;
var number2 = 5.012;
var number3 = 50000000.0126666;

console.log("Adding numbers: " + number1 + " + " + number2 + " = " + number1 + number2); // this will not work
console.log("Adding numbers: " + number1 + " + " + number2 + " = " + (number1 + number2));
console.log(number3 + 0.15);

console.log(""); console.log("__ TOPIC: BASIC OPERATORS __");
var boolean1 = true;
var boolean2 = false;
console.log(">> " + 1 % 2); // prints 1
console.log(">> " + (boolean1 && boolean2)); // prints what? T & T -> T ;; T & F -> F
console.log(">> " + (boolean1 || boolean2)); // prints what?

console.log(""); console.log("__ TOPIC: COMPARISON OPERATOR __");
// == VS ===
// In JavaScript there are two comparison operators. If
//  we want to compare values we use == operator. If we
//  want to compare values and types we use ===.
console.log(5 == 5); // prints true
console.log("5" == 5); // prints true
console.log("5" === 5); // prints false (types differ)


console.log(""); console.log("___ TOPIC: Conditionals ___");
var first = 10;
var second = 20;

if(first > second){
    console.log("First is bigger than second!");
} else {
    console.log("First is smaller than second!");
}

console.log(""); console.log("___ TOPIC: Loops ___");
for(var i = 0; i < 3; i++) {
    console.log("Inside for() loop, i is now:" + i);
}

console.log(""); console.log("___ TOPIC: Arrays ___");
var myAwesomeArray = [1, 4, 7, 9];

console.log(myAwesomeArray[0]); // prints 7, since arrays are 0 based
console.log(myAwesomeArray.length); // prints 3
console.log(myAwesomeArray[myAwesomeArray.length - 1]); // gets the last element
console.log(myAwesomeArray)

console.log(""); console.log("___ TOPIC: Array push() and pop() ___");
myAwesomeArray.push(155); // arrays are dynamic so we can extend them with additional elements
console.log(myAwesomeArray.length); // prints 5
console.log(myAwesomeArray[myAwesomeArray.length - 1]); // gets the last element
console.log(myAwesomeArray.pop())
console.log(myAwesomeArray)
console.log(myAwesomeArray.pop())
console.log(myAwesomeArray)

// arrays have a lot of functionality associated, see: https://www.w3schools.com/jsref/jsref_obj_array.asp
// ... like map, filter, reduce (what JAVA's streams provide as well)
var personel = [
    {
        id: 51,
        name: "Joana",
        baseSalary: 450,
        bonus: true,
        gender: "female"
    },
    {
        id: 2,
        name: "Jonas",
        baseSalary: 600,
        bonus: false,
        gender: "male"
    },
    {
        id: 432,
        name: "Marta",
        baseSalary: 1005,
        bonus: true,
        gender: "female"
    },
    {
        id: 44,
        name: "Petras",
        baseSalary: 450,
        bonus: true,
        gender: "male"
    }
];

// Can we have an array of mixed datatypes?
var mixedArray = ["A", 1, "Boo", true, ["Array", "Array"], {x: "y"}];
for(var i = 0; i < mixedArray.length; i++) {
    console.log("Type of " + mixedArray[i] + " = " + (typeof mixedArray[i]));
}

// inserting / removing at random position with .splice()
console.log(""); console.log("___ TOPIC: Array.splice() ___");
mixedArray.splice(2, 2);
for(var i = 0; i < mixedArray.length; i++) {
    console.log("Type of " + mixedArray[i] + " = " + (typeof mixedArray[i]));
}

console.log([0,1,2,3,4,5,6].splice(2,3));

console.log(""); console.log("___ TOPIC: Array.filter() ___");
// myArr.filter(func(){});
var males = personel.filter(function (person) {
    if (person.gender === "male") return person;
});
console.log("Male personnel:"); console.log(males);


console.log(""); console.log("___ TOPIC: Objects ___");
// Object literal - one way to create objects
var cat = {
    weight: 12.2,
    name: "Fluffy",
    fur: false
};

// Two ways to access the properties
console.log(cat.weight);
console.log(cat.name);
console.log(cat.fur);

// Another way to access properties. This
// way is useful because you can use valid strings
// that are NOT VALID variable names to give to your objects
// maybe you received them from user input or some file
console.log(cat["weight"]);
console.log(cat["name"]);
console.log(cat["fur"]);

// We can add a property to our object afterwards
console.log(cat);
cat.age = 4;
console.log(cat.age);
console.log(cat); // cat property is now part of the object itself!
console.log(Object.keys(cat));

// Adding functions / behavior to our objects
// ... we can also do it after we created them
// ... this is very hard to do in java, but in
// ... dynamically typed languages it is easy
cat.speak = function(){ console.log("Meow!") };
cat.speak();
console.log(Object.keys(cat));


console.log(""); console.log("___ TOPIC: Functions ___");

// inbuilt functions:
// https://www.w3schools.com/jsref/jsref_obj_global.asp

// function declaration
function myFunction(firstParam, secondParam){
    return firstParam + secondParam;
}

// function invocation - calling the function
console.log(myFunction(1, 2))
console.log(myFunction("a", "b"))

// Default parameters to functions
function greet_a_person(name = "Mindaugas"){
    console.log("Hello " + name + "!");
}
greet_a_person("Bart");


console.log(""); console.log("___ TOPIC: ES 6 ___");

let square = (x) => {
    return x * x;
}
console.log(square(5))


console.log(""); console.log("___ TOPIC: Array destructuring ___");
var introduction = ["Hello", "I" , "am", "Sarah"];
var [greeting, pronoun, third] = introduction;
console.log(greeting);

console.log(""); console.log("___ TOPIC: Spread operator ___");
const myArray = [`🤪`,`🐻`,`🎌`];
const yourArray = [`🙂`,`🤗`,`🤩`,`🤑`];
const ourArray = [...myArray,...yourArray];
console.log(ourArray);

const c = 5;
c = 6; // TypeError: Assignment to constant variable.

console.log(c);


