package cf.mindaugas.backend;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static spark.Spark.get;
import static spark.Spark.post;

public class Api {
    public static void main(String[] args) {
        get("/", (request, response) -> {
            return "<ul>\n" +
                    "  <li><a href=\"/hw\">Hello world!</a></li>\n" +
                    "  <li><a href=\"/form\">Form page</a></li>\n" +
                    "</ul>";
        });

        // 0. Generating simple HTML
        // http://localhost:4567/hw
        get("/hw", (request, response) -> {
            return "<h1>Hello <span style=\"color: red\">world</span>!!!</h1>" + request.ip();
        });

        get("/form", (request, response) -> {
            // Reference to the HTML file containing the form
            String htmlFile = "C:\\Users\\Greta\\" +
                    "Desktop\\Projects\\JavaVln5HtmlCssJs\\" +
                    "src\\main\\resources\\01_html\\form.html";

            // Converting the contents of the file
            String htmlContent = new String(Files.readAllBytes(Paths.get(htmlFile)), StandardCharsets.UTF_8);

            // System.out.println(htmlContent);
            return htmlContent;
        });

        post("/form", (request, response) -> {
//            String name = request.body().split("&")[0];
//            String surname = request.body().split("&")[1];
//            return name.split("=")[1] + " : " + surname.split("=")[1];
            return "<h1>Hi, " + request.body().split("=")[1] + "</h1>";
        });
    }
}
